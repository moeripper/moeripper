# Moeripper

## Introduction

Moeripper is a DVD ripping solution for bulk ripping of DVDs. In cases where the ripping shall be an automated process Moeripper becomes handy with its batch processing of ripping jobs.

By using Moeripper it is easy to rip DVDs with multiple audio tracks into one output file (using the Matroska container format). Although it is a program designed for the unix shell it has a wizard to help you create a new ripping job.

Moeripper lets you define profiles where each profile represents a set of audio and video settings. With these profiles you can also create multiple audio or video tracks out of one track. This might be useful when you want one "high quality" video stream and a second "low quality" stream for old computers. Of course, the same thing is possible for audio streams.


## Installation

### Requirements

The operating systems must meet the following requirements:

- Unix or Unix-like operating system (like Linux)
- Ruby > 1.9


Moeripper requires the following programs to run properly:

- mpv
- ffmpeg
- normalize-audio
- vorbisgain
- mp3gain
- mkvmerge (from mkvtoolnix)

To be able to rip a DVD one should have at least 20 GiB of free space available. This space is needed for temporary files during the ripping process.

### Installation itself

Moeripper does not need to be installed somewhere in the file system. You can just place the moeripper.rb script in a directory where you want to have the temporary files from DVD ripping and Moeripper will work just fine after you copied the configuration files (see below).

However, if you feel more comfortable with it you may place the moeripper.rb script into /usr/bin or any other directory for executable files.

You may also place the moeripper.rb script into the directory $HOME/bin, if you have your $PATH variable configured so that it includes the $HOME/bin directory in the search for executable files. Depending on your shell the configuration file is in another configuration file in your HOME directory.

### Copying configuration files

Before you can start using Moeripper you need to copy the configuration files into the folder $HOME/.config/moeripper/. At the moment the configuration consists of the file Profiles.csv which contains all defined profiles for audio and video conversion. 

In case you just want to get Moeripper working you may execute the script install-profiles.sh in your copy of the Moeripper source code repository. It will create the directory $HOME/.config/moeripper/ for your and copy the Profiles.csv file in there.


That's it, Moeripper is ready to rip :)
