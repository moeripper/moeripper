#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    moeripper.py,
#    Copyright (C) 2014  Moritz Strohm <ncc1988@posteo.de>
#    
#    This file is part of moeripper - a dvd ripping solution
#    
#    moeripper is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    
#    moeripper is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with moeripper.  If not, see <http://www.gnu.org/licenses/>.
#


#this program requires the following software packages and programs:
# - mplayer
# - avconv (package libav-tools or ffmpeg), with mpeg4 and libvorbis support
# - normalize-audio
# - vorbisgain
# - mkvmerge (mkvtoolnix)
#
#You can test if these programs are installed by calling moeripper.rb with the parameter checkinstall:
# ./moeripper.rb checkinstall
from os.path import expanduser, isfile
import sys

class ProfileDefinition:
  def __init__(self, profileLine):
    profileLine = profileLine.split(';')
    if(len(profileLine) == 13):
      #correct length of a profile line
      self.name = profileLine[0]
      self.description = profileLine[1]
      self.videoResolution = profileLine[2]
      self.videoFPS = profileLine[3]
      self.videoFrameConfiguration = profileLine[4]
      self.videoCodec = profileLine[5]
      self.videoQuality = profileLine[6]
      self.videoFourCC = profileLine[7]
      self.audioSamplerate = profileLine[8]
      self.audioChannels = profileLine[9]
      self.audioCodec = profileLine[10]
      self.audioQuality = profileLine[11]
      #Debug:
      #print "DEBUG: new profile created!"
      #print "Name: "+self.name+", audio quality: "+self.audioQuality
    else:
      print "ERROR: Profile line not in the right format! ("+str(len(profileLine))+" fields instead of 13!"


class AudioChannelDefinition:
  def __init__(self, audioDefinitionString):
    audioDefinitionString = audioDefinitionString.split('=')
    self.languageCode = audioDefinitionString[0]
    if(self.ConvertLanguageCode == None):
      print "AudioChannelDefinition Error: Language Code " + self.languageCode + " is unknown!"
      raise Exception(self.languageCode) #UnsupportedLanguageError(self.languageCode)
    self.audioID = audioDefinitionString[1]
    self.ffmpegStreamNr = audioDefinitionString[2]
  
  def ConvertLanguageCode(self):
    for i in range(0,len(self.LanguageCodeMap)-1):
      if self.LanguageCodeMap[i][0] == self.languageCode:
        return self.LanguageCodeMap[i][1]
    #we have reached the end of the loop without a match: return None
    return None
  
  
  LanguageCodeMap=[
    ["unknown", "und"], #undefined language
    
    ["de","ger"],
    ["en","eng"],
    ["fr","fre"],
    ["es","spa"],
    ["it","ita"],
    ["ja","jpn"],
    ["ru","rus"],
    ["cs","cze"],
    ["pl","pol"],
    ["hu","hun"],
    ["pt","por"]
  ]


class JobManager:
  def __init__(self):
    self.jobs = []
    self.jobFile = None
    self.jobsProcessed = 0
    self.profiles = []
  
  def AddProfile(self,profile):
    if(isinstance(profile,ProfileDefinition)):
      self.profiles.append(profile)
    else:
      print "Moeripper.AddProfile: ERROR: Invalid profile definition!"
  
  
  
  def ReadProfilesFromFile(self, profileFileName):
    if(isfile(profileFileName)):
      #read file version:
      
      profilesFile = open(profileFileName, 'r')
      profileFileVersion = profilesFile.readline().rstrip()
      
      if(profileFileVersion == '@v1'):
        #supported version: read profiles
        profileLine = profilesFile.readline() #jump over table description line
        
        while profileLine:
          profileLine = profilesFile.readline()
          if(len(profileLine) != 0): #we don't want to process empty lines
            self.AddProfile(ProfileDefinition(profileLine))
          
      
        #all profiles read here
      else:
        if(profileFileVersion == "@v0"):
          print "ERROR: Your profiles file is outdated! Please run \"moeripper convertprofiles\" to correct this!"
        else:
          print "ERROR: Profiles file is in an unsupported version!"
      
    else:
      print "ERROR: No profiles file given! Cannot continue!\n"
      print "Please create a profile by hand or by using moeripper-profiles.rb\n"
      print "The profiles file should be "+profileFileName
  
  
  def ConvertProfileFile(self, profileFileName):
    newProfileContent = ''
    if(isfile(profileFileName)):
      profilesFile = open(profileFileName, 'r')
      #read file version:
      profileFileVersion = profilesFile.readline().rstrip()
      if(profileFileVersion == '@v0'):
        profileDefinition = profilesFile.readline() #jump over table description line
        #supported old version: read profiles and add the profile description field
        newProfileContent = '@v1\n'
        newProfileContent += 'ProfileName;ProfileDescription;VideoResolution;VideoFPS;VideoFrameConfig;VideoCodec;VideoQuality;VideoFourCC;AudioSamplerate;AudioChannels;AudioCodec;AudioQuality;\n'
        
        while profileDefinition:
          profileDefinition = profilesFile.readline().rstrip().split(';')
          newProfileContent += profileDefinition[0] + ';;' #leave description empty
          newProfileContent += profileDefinition[1] + ';'
          newProfileContent += profileDefinition[2] + ';'
          newProfileContent += profileDefinition[3] + ';'
          newProfileContent += profileDefinition[4] + ';'
          newProfileContent += profileDefinition[5] + ';'
          newProfileContent += profileDefinition[6] + ';'
          newProfileContent += profileDefinition[7] + ';'
          newProfileContent += profileDefinition[8] + ';'
          newProfileContent += profileDefinition[9] + ';'
          newProfileContent += profileDefinition[10] + ';'
      
        profilesFile.close()
        try:
          subprocess.check_call(['mv', profileFileName, profileFileName+'.bak'])
        except CalledProcessError:
          print "ERROR: Cannot move old profiles file! Check the privileges on "+profileFileName+"and try again!"
          return false
        
        profilesFile = open(profileFileName, 'w')
        for p in newProfileContent:
          profilesFile.write("%s\n" % p)
        profilesFile.close()
        print "Conversion of old profiles file (v0 -> v1) finished!"
      else:
        print "ERROR: Profiles file is in an unsupported version and cannot be converted!"
    else:
      print "ERROR: No profiles file given! Cannot continue!\n"
      print "Please create a profile by hand or by using moeripper-profiles.rb\n"
      print "The profiles file path should be "+profileFileName
  
  
  
  def CreateJob(self, jobLine):
    jobLine = jobLine.split(';')
    steps = jobLine[0].split()
    if((len(steps) > 2) or (len(steps) == 0)):
      print "JobManager Error: Number of steps in Jobs are neither 1 or 2. More than two steps are not supported!"
      return False
      
    profileName = jobLine[1]
    if(profileName == ''):
      print "JobManager Error: Profile name must not be empty!"
      return False
    
    
    i = 0
    while (i < (len(self.profiles)-1)) and (self.profiles[i].name != profileName):
      i = i+1
    
    if(i == (len(self.profiles)-1)):
      #profile not found
      print "JobManager Error: No known profile named "+profileName+"!"
      return False
    profile = self.profiles[i]
    
    dvdName = jobLine[2]
    if(dvdName == ''):
      print "JobManager Error: DVD name must not be empty!"
      return False
    
    dvdTrack = jobLine[3]
    if(dvdTrack == ''):
      print "JobManager Error: DVD track must not be empty!"
      return False
    
    title = jobLine[4]
    if(title == ''):
      print "JobManager Error: Title must not be empty!"
      return False
    
    index = jobLine[5] #the index may be empty, so no checking done here
    videoDefinition = jobLine[6].split(',')
    if(len(videoDefinition) < 1):
      print "JobManager Error: Video configuration must not be empty!"
      return False
    
    audioOrderStrings = jobLine[7].split(',')
    if(len(audioOrderStrings) < 1):
      print "JobManager Error: Audio order must not be empty!"
      return False
    
    audioOrder = []
    for i in range(0,len(audioOrderStrings)-1):
      try:
        audioOrder.append(AudioChannelDefinition(audioOrderStrings[i]))
      except Exception as e: #UnsupportedLanguageError:
        print "JobManager Error: Unsupported language code in audio configuration! Language Code:" + e.msg
        return False
    
    self.jobs.append(Job(self, steps, profile, dvdName, dvdTrack, title, index, videoDefinition, audioOrder))
    return True 
  
  
  def ExecuteJobs(self):
    if(len(self.jobs) == 0):
      print "JobManager Error: No jobs present!"
      return False
    
    return True
    
  def ExecuteJob(self):
    finishCurrentJob()
  
  def UpdateJob(self, job):
    i = 0
    while (i < len(self.jobs)-1) and (job != self.jobs[i]):
      i = i+1
    
    if(i == len(self.jobs)-1):
      #job not found
      print "ERROR: update job: Job not found!"
      return False
    
    
  
  def ReadJobsFromFile(self, jobFileName, numberOfJobs):
    if(not isfile(jobFileName)):
      print "ERROR: job file ("+jobFileName+") not found in this directory!"
      return False
    
    self.jobFile = open(jobFileName,'r')
    
    #line 1:
    if(not self.jobFile.readline()):
      print "ERROR: job file ("+jobFileName+") is empty!"
      if(self.jobsProcessed == 0):
        print "If you like to have assistance in creating a moeripper job please execute \"python moeripper.py wizard\""
      else:
        print "The job file wasn't empty when the script started. Please make sure that no other program is accessing the job file."
      return False
    
    #read the first job in the file:
    jobLine = self.jobFile.readline()
    if(not jobLine):
      if(self.jobsProcessed == 0):
        print "ERROR: todo file ("+jobFileName+") has no jobs!"
        print "If you like to have assistance in creating a moeripper job please execute \"python moeripper.py wizard\""
      else:
        print "No more jobs in job file. Nothing left to do. Have a nice day!"
      return False
    
    jobFields = jobLine.rstrip().split(';')
    if(len(jobFields) != 8):
      print "ERROR: todo file ("+jobFileName+") is not in the recognised format!"
      return False
    
    try:
      self.CreateJob(jobFields)
    except JobCreationError:
      #if(preprocessCurrentJob() != true)
      print "ERROR: current job is invalid!"
      print "If you like to have assistance in creating a moeripper job please execute \"python moeripper.py wizard\""
      return False
    
    print "################"
    print "JOB INFORMATIONS:"
    print "Mode:\t\t",
    jobSteps = self.currentJob.GetSteps()
    for i in range(0,len(jobSteps)-1):
      if(i > 0):
        print " -> ",
      end
      if(jobSteps[i] == 'R'):
        print "Rip",
      elif(jobSteps[i] == "r"):
        print "Rip and wait",
      elif(jobSteps[i] == "C"):
        print "Convert",
      elif(jobSteps[i] == "c"):
        print "Convert and wait",
      else:
        print "UNKNOWN"
    
    
    print ''
    print "Profile:\t"+self.currentJob.profile.name
    print "DVD name:\t"+self.currentJob.dvdName
    print "DVD track:\t"+self.currentJob.dvdTrack
    print "Title:\t\t"+self.currentJob.title
    print "Index:\t\t"+self.currentJob.index
    print "----------------"
    print "Video configuration:"
    print "Aspect ratio:\t\t"+self.currentJob.getVideoAspectRatio()
    
    if(self.currentJob.profile.videoFrameConfiguration == 'p'):
      print "Frames:\t\t\tprogressive"
    elif(self.currentJob.profile.videoFrameConfiguration == "i"):
      print "Frames:\t\t\tinterlaced"
    elif(self.currentJob.profile.videoFrameConfiguration == "s"):
      print "Frames:\t\t\tprogressive with fields swapped"
    else:
      print "Frames:\t\t\tunknown"
    
    print "Frames per second:\t"+self.currentJob.profile.videoFPS
    print "----------------"
    print "Audio track order:\t"
    for i in range(0,len(self.currentJob.audioOrder)-1):
      if(i > 0):
        print ",",
      print self.currentJob.audioOrder[i].languageCode,
    print ''
    print "################"




class Job:
  def __init__(self, jobManager, steps, profile, dvdName, dvdTrack, title, index, videoDefinition, audioOrder):
    self.jobManager = jobManager
    self.steps = steps
    self.profile = profile
    self.dvdName = dvdName
    self.dvdTrack = dvdTrack
    self.title = title
    self.index = index
    self.videoDefinition = videoDefinition
    self.audioOrder = audioOrder
    
  def GetVideoAspectRatio(self):
    return self.videoDefinition[0]
    
  #@classmethod
  #def fromJobLine(cls, jobline):
  #  jobLine = jobLine.split(';')
  #  return cls(steps,profileName)
    
    
  def GetSteps(self): #returns all steps for this job
    return self.steps
  
  def GetDirectoryName(self):
    return self.dvdName+'-'+self.dvdTrack
  
  def ExecuteStep(self):
    if(len(self.steps) == 0):
      return False
    
    status = False
    if (self.steps[0].upper() == 'R'):
      if(self.Rip() != True):
        print "ERROR when ripping!"
        return False
    if (self.steps[0].upper() == 'C'):
      if(self.Convert() != True):
        print "ERROR when converting!"
        return False
    
    if(self.steps[0].isLower()):
      #the job is written with a lowercase
      #confirmation is required before the job is finished:
      input("Press enter to continue...")
      self.steps.pop(0)
      
    #update the job list:
    #in case something goes wrong in the next step
    #we don't have to redo the ripping process
    self.jobManager.UpdateJob(self)
  
  def ToCsv(self):
    csvLine = (''.join(self.steps) +';'+
      self.profile.name+';'+
      self.dvdName +';'+
      self.dvdTrack +';'+
      self.title +';'+
      self.index +';'+
      self.videoDefinition[0]+';'
      )
    for i in range(0,len(self.audioOrder)-1):
      print "L="+self.audioOrder[i]
      if(i > 0):
        csvLine += ","
        
      csvLine += (self.audioOrder[i][0]+'='+
        self.audioOrder[i][1]+'='+
        self.audioOrder[i][2]+',')
    csvLine += ';'
    return csvLine


class Moeripper:
  
  
  def __init__(self):
    self.profiles = []
    self.CONFIG_DIR = expanduser("~")+"/.config/moeripper/"
    self.PROFILES_PATH = self.CONFIG_DIR + "Profiles.csv"
    self.JOBFILE = "moeripper.todo"
    self.jobManager = JobManager()
    
  def CheckPrerequesites(self):
    try:
      subprocess.check_call(['mplayer', '-v'], stderr=subprocess.STDOUT)
      print "mplayer:\t\tinstalled!"
    except CalledProcessError:
      if (CalledProcessError.returncode == 127):
        print "mplayer:\t\t not installed"
    
    try:
      subprocess.check_call(['avconv', '-version'], stderr=subprocess.STDOUT)
      print "avconv:\t\tinstalled"
    except CalledProcessError:
      if(CalledProcessError.returncode == 127):
        print "avconv:\t\tnot installed"
    
    try:
      subprocess.check_call(['normalize-audio', '--version'], stderr=subprocess.STDOUT)
      print "normalize-audio:\tinstalled"
    except CalledProcessError:
      if(CalledProcessError.returncode == 127):
       print "normalize-audio:\tnot installed"
    
    try:
      subprocess.check_call(['vorbisgain', '--version'], stderr=subprocess.STDOUT)
      print "vorbisgain:\tinstalled"
    except CalledProcessError:
      if(CalledProcessError.returncode == 127):
        print "vorbisgain:\tnot installed"
    
    try:
      subprocess.check_call(['mp3gain', '-v'], stderr=subprocess.STDOUT)
      print "mp3gain:\t\tinstalled"
    except CalledProcessError:
      if(CalledProcessError.returncode == 127):
        print "mp3gain:\t\tnot installed"
    
    try:
      subprocess.check_call(['mkvmerge', '--version'], stderr=subprocess.STDOUT)
      print "mkvmerge:\t\tinstalled"
    except CalledProcessError:
      if(CalledProcessError.returncode == 127):
        print "mkvmerge:\t\tnot installed"
  
  
  def Setup(self):
    self.jobManager.ReadProfilesFromFile(self.PROFILES_PATH)
  
  def ExecuteJobs(self):
    status = True
    while(status == True):
      if(self.jobManager.ReadJobsFromFile(self.JOBFILE,1) == False):
        #no more jobs
        return True
      status = self.jobManager.ExecuteJobs()
    return status
  

  def PrintHelp(self):
    print
    print "Usage: moeripper.rb OPTION"
    print "  where OPTION is one of the following:"
    print
    print "  convertprofiles"
    print "   – converts old profile files to the current version"
    print
    print "  checkinstall"
    print "   – checks if all necessary programs are installed"
    print
    print "  help"
    print "   – displays this help"
    print
    print "  wizard"
    print "   – starts a wizard to ease the process of adding ripping jobs to the joblist"
    print
    print

  

def Main(argv):
  doWizard = False
  mr = Moeripper()
  
  if(len(argv) == 1):
    if(argv[0] == 'convertprofile'):
      mr.ConvertProfileFile()
      return 0
    if(argv[0] == 'checkinstall'):
      return mr.CheckPrerequesites()
    if(argv[0] == 'wizard'):
      doWizard = True
    if(argv[0] == 'help'):
      mr.ShowHelp()
      return 0
    
  else:
    if(len(argv) > 1):
      print "ERROR: too much arguments!"
      return 1
  
  mr.Setup()
  
  if(doWizard == True):
    mr.Wizard()
    return 0
  
  
  #do all jobs until there are no more
  if(mr.ExecuteJobs() != True):
    print "Error when processing jobs!"
    return 1

def Test(argv):
  jm = JobManager()
  jm.ReadProfilesFromFile(expanduser("~")+"/.config/moeripper/Profiles.csv")
  jm.CreateJob("rc;DVD_PAL;test;1;testtitel;0-00;4:3;en=128=1,de=129=2;")
  
  print jm.jobs[0].ToCsv()


if __name__ == '__main__':
  #sys.exit(Main(sys.argv))
  sys.exit(Test(sys.argv))